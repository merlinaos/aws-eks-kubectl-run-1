# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.3.1

- patch: Internal maintenance: bump pipe-release.

## 1.3.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.2.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.2.3

- patch: Internal maintenance: Add gitignore secrets.

## 1.2.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.2.1

- patch: Added more details about Kubernetes access controls to the README
- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.2.0

- minor: Add default values for AWS variables.

## 1.1.1

- patch: Internal maintenance: Add check for newer version.

## 1.1.0

- minor: The pipe now accepts directory path in RESOURCE_PATH variable

## 1.0.0

- major: Changed the SPEC_FILE varialbe to RESOURCE_PATH

## 0.2.1

- patch: Documentation improvements.

## 0.2.0

- minor: Fixed the docker image reference

## 0.1.0

- minor: Initial release
- patch: Updated the base pipe version

